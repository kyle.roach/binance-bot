# 
# main.py
#

import time
import signal

import config
import processor

config.market_pairs_list = ['BTCUSDT', #0
                            'ETHUSDT', #1
                            'LTCUSDT', #2
                            'BNBUSDT', #3
                            'XRPUSDT', #4
                            'EOSUSDT', #5
                            'ETCUSDT', #6
                            'BCHUSDT', #7
                            'TRXUSDT', #8
                            'ADAUSDT'] #9
    
config.asset_list = ['USDT',
                     'BTC',
                     'ETH',
                     'LTC',
                     'BNB',
                     'XRP',
                     'EOS',
                     'ETC',
                     'ETC',
                     'BCH',
                     'TRX',
                     'ADA']


def my_signal_term_handler(sig, frame):
    config.termination_requested = True

def main():

    signal.signal(signal.SIGINT, my_signal_term_handler)
    
    config.start_time = time.time()
    while not config.termination_requested:
        processor.mainLoop()
    config.end_time = time.time()
    
    print 'exiting .. time elapsed = ' + str(config.end_time - config.start_time)

if __name__ == "__main__":
    main()


