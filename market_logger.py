# 
# market_logger.py
#

import shutil
import time
import math
import os.path
import simplejson as json

import api
import config

def update_configs():
    
    # check if json data exists for given market pair
    for market_pair in config.market_pairs_list:
        json_file = 'data/' + market_pair + ' data.json'
        if not os.path.exists(json_file):
            update_json_database()
            break
        
    # check if json data has not been updated in 24h
    for market_pair in  config.market_pairs_list:
        json_file = open('data/' + market_pair + ' data.json')
        market_data = json.load(json_file)
        json_file.close()  
        if market_data['time'] < time.time()-config.TWENTY_FOUR_HOURS :
            update_json_database()
            break
            
    # now update configs
    for market_pair in config.market_pairs_list:
        json_file = open('data/' + market_pair + ' data.json')
        market_data = json.load(json_file)  
        
        config.minimum_quantity[market_pair] = float(market_data['minimum_quantity'])
        config.minimum_order_size[market_pair] = float(market_data['minimum_order_size'])
        config.step_size[market_pair] = float(market_data['step_size'])
        config.standard_deviation[market_pair] = float(market_data['standard_deviation'])
        config.ten_day_moving_average[market_pair] = float(market_data['moving_average'])
        config.time_of_json_update[market_pair] = float(market_data['time'])
        json_file.close()
        
    # update candlesticks
    config.candle_updated = False
     
    # initialization
    if config.candle_open_time == 0:
        print time.ctime(time.time())
        config.candle_open_time = time.time()
        for market_pair in config.market_pairs_list:
            config.candle_open_price[market_pair] = api.get_market_price(market_pair)
            print market_pair + ':' + str(config.candle_open_price[market_pair])

    # if it has been ten minutes, update current candle
    if config.candle_open_time < (time.time()-config.TEN_MINUTES):
        print '\n'
        print time.ctime(time.time())
        config.candle_open_time = time.time()
        config.last_candle = config.current_candle.copy()
        for market_pair in config.market_pairs_list: 
            config.candle_close_price[market_pair] = api.get_market_price(market_pair)
            config.current_candle[market_pair] = (config.candle_close_price[market_pair]-config.candle_open_price[market_pair])/config.candle_open_price[market_pair]
            config.candle_open_price[market_pair] = config.candle_close_price[market_pair]
            print market_pair + ':' + str(config.candle_open_price[market_pair])
            
        config.candle_updated = True
        
    # if candles have been updated then we will update the asset balances
    if config.candle_updated:
        for market_asset in config.asset_list:
            config.qty_asset[market_asset] =  api.get_balance(market_asset)
            
    # if candle have been updated then will also check if we can transfer dust
#     if api.get_last_dust_transfer_time() < time.time()-TWENTY_FOUR_HOURS: # if it has been 24h since last transfer set to True
#         config.transfer_dust = True
            
    
def update_json_database():
    print 'Updating JSON database ..'
    
    for market_pair in config.market_pairs_list:
        
        json_file_path = 'data/' + market_pair + ' data.json'
        data_dir = 'data/'
        log_dir = 'data/logs'
        log_pair_dir = 'data/logs/' + market_pair
        
        # if the data directory does not exist yet we will make that now
        if not os.path.exists(data_dir):
            os.mkdir(data_dir)
        
        # if the log directory does not exist yet we will make that now
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)        
        
        # if the pair directory does not exist yet we will make that now
        if not os.path.exists(log_pair_dir):
            os.mkdir(log_pair_dir)
        
        # if the json file exists we will move it to the log directory
        if os.path.exists(json_file_path):
            json_file = open(json_file_path)
            market_data = json.load(json_file)  
            shutil.move(json_file_path, log_pair_dir + '/' + market_pair + ' ' + market_data['local time'] + '.json')
            json_file.close()
            
        # now we will update 
        json_file = open(json_file_path, 'w')
        
        json_data = {}
        
        json_data['minimum_order_size'] = api.get_min_order_size(market_pair)
        json_data['minimum_quantity'] = api.get_minimum_quantity(market_pair)
        json_data['step_size'] = api.get_step_size(market_pair)
        
        historical_data = api.get_historical_klines_10_DAYS(market_pair)
    
        # get opening price for the last ten days
        sum_of_prices = 0   
        for day in range(10):
            json_data['day_' + str(day) + '_price'] = historical_data[day][4] #1=open, 2=high 3=low 4=close 5=volume
            sum_of_prices += float(json_data['day_' + str(day) + '_price'])
       
        # get deviation for each of the last ten days
        sum_of_squared_deviation = 0      
        for day in range(10): 
            squared_deviation = pow(float(json_data['day_' + str(day) + '_price'])-((sum_of_prices/10)), 2)  
            sum_of_squared_deviation += squared_deviation
        
        json_data['moving_average'] = sum_of_prices/10
        json_data['standard_deviation'] = math.sqrt(sum_of_squared_deviation/10)     
        
        json_data['time'] = time.time()
        json_data['local time'] = time.ctime(time.time())  
        
        json.dump(json_data, json_file, indent = 4)
        json_file.close()
        

def update_asset_log():
    
    if (config.time_hourly_update < (time.time() - config.ONE_HOUR)):
        config.time_hourly_update = time.time()
	asset_log = open('data/asset_log.txt', 'a')
        
        value_usd = 0
        for market_asset in config.asset_list:
            if market_asset == 'USDT':
                value_usd += api.get_balance(market_asset)
            else:
                value_usd += api.get_balance(market_asset)*api.get_market_price(market_asset + 'USDT')
        
        # if this is the first interval we will initialize with the hourly and daily usd values
        if config.usd_value_hourly_update == 0:
            config.usd_value_hourly_update = value_usd
            config.usd_value_daily_update = value_usd
            return
        
        change_in_value = (value_usd - config.usd_value_hourly_update)/config.usd_value_hourly_update
        config.usd_value_hourly_update = value_usd
        
	output = time.ctime() + ' balance: ' + str(value_usd) + ', %change: ' + str(change_in_value) + '%\n'
        asset_log.write(output)
        
        if (config.time_daily_update < (time.time() - config.TWENTY_FOUR_HOURS)):
	    config.time_daily_update = time.time()
            change_in_value = (value_usd - config.usd_value_daily_update)/config.usd_value_daily_update
            config.usd_value_daily_update = value_usd
	    output = '******************** 24H Net Change = ' + str(change_in_value) + '% ***************\n'
            asset_log.write(output)
           
        
        asset_log.close()
        
