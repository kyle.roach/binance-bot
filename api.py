# 
# api.py
#

import config
import math
import datetime
from time import sleep

from binance.client import Client
api_key = 'QDVjVEjjgpDPQviCEziyiiKlb8lexrXgyzrjwxj9Vdfrka4b5EOArSvFzGbRSFWh'
api_secret = 'pvF4Nq4eMpdXVVY29n1MgtGeJ38lXssbV6t64hGXW66ugA5lpOoafdIKInShR4tR'
client = Client(api_key, api_secret, {'timeout': 20})
import requests.exceptions
import binance.exceptions

MAX_RETRIES = 100

def get_historical_klines_10_DAYS(market_pair):
    for _ in range(MAX_RETRIES):
        try: 
            historical_data = client.get_historical_klines(market_pair, Client.KLINE_INTERVAL_1DAY, '10 DAYS AGO UTC')
            return historical_data
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)

def get_min_order_size(market_pair):
    for _ in range(MAX_RETRIES):
        try:
            market_info = client.get_symbol_info(symbol=market_pair)
            min_order_size = market_info['filters'][3]['minNotional'] # currently 10USD
            return min_order_size
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)
    
def get_minimum_quantity(market_pair):
    for _ in range(MAX_RETRIES):
        try:
            market_info = client.get_symbol_info(symbol=market_pair)
            minimum_quantity = market_info['filters'][2]['minQty']
            return minimum_quantity  
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)
    
def get_step_size(market_pair):
    for _ in range(MAX_RETRIES):
        try:
            market_info = client.get_symbol_info(symbol=market_pair)
            step_size = market_info['filters'][2]['stepSize']
            return step_size
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)

def get_balance(market_asset):
    for _ in range(MAX_RETRIES):
        try:
            balance_info = client.get_asset_balance(asset=market_asset)
            balance =  float(balance_info['free']) + float(balance_info['locked'])
            return balance
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)

def get_market_price(market_pair):
    for _ in range(MAX_RETRIES):
        try:
            market_info = client.get_symbol_ticker(symbol=market_pair)
            market_price = float(market_info['price'])
            return market_price
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)

def get_last_dust_transfer_time():
    for _ in range(MAX_RETRIES):
        try:
            last_dust_transfer = 0
            dust_log = client.get_dust_log()
            for dust_transfer in dust_log['results']['rows']:
                date_str = dust_transfer['operate_time']
                dt = datetime.datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S')
                epoch =datetime.datetime.utcfromtimestamp(0)
                if((dt - epoch).total_seconds() > last_dust_transfer):
                    last_dust_transfer = (dt - epoch).total_seconds()
                    
            return last_dust_transfer
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)

def transfer_dust(market_asset):
    for _ in range(MAX_RETRIES):
        try:
            order = client.transfer_dust(asset=market_asset)
            return order
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)

def market_buy(market_pair, quantity):
    for _ in range(MAX_RETRIES):
        try:
            dec_pts = int(math.log(1/config.step_size[market_pair], 10))
            formatted_qty = str(round(quantity, dec_pts))
            order = client.order_market_buy(symbol=market_pair, quantity=formatted_qty)
            return order
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)
    
    
def market_sell(market_pair, quantity):
    for _ in range(MAX_RETRIES):
        try:
            dec_pts = int(math.log(1/config.step_size[market_pair], 10))
            formatted_qty = str(round(quantity, dec_pts))
            order = client.order_market_sell(symbol=market_pair, quantity=formatted_qty)
            return order
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)


def set_stop_loss(market_pair, stop_price, quantity):
    for _ in range(MAX_RETRIES):
        try:
            dec_pts = int(math.log(1/config.step_size[market_pair], 10))
            formatted_qty = str(round(quantity, dec_pts))    
            order = client.create_order(symbol=market_pair, 
                                        side=client.SIDE_SELL, 
                                        quantity=formatted_qty,
                                        price=stop_price,
                                        type=client.ORDER_TYPE_STOP_LOSS_LIMIT,
                                        timeInForce=client.TIME_IN_FORCE_GTC,
                                        stopPrice=stop_price)
            return order
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)
        except binance.exceptions.BinanceAPIException:
            print 'stop loss price too high, not setting stop loss'
            return -1

def get_open_orders(market_pair):
    for _ in range(MAX_RETRIES):
        try:
            open_orders = client.get_open_orders(symbol=market_pair)
            return open_orders
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)
        
def cancel_open_order(market_pair, order_id):
    for _ in range(MAX_RETRIES):
        try:
            order = client.cancel_order(symbol=market_pair,orderId=order_id)
            return order
        except requests.exceptions.RequestException:
            print 'A timeout has occured!'
            sleep(1)
        except binance.exceptions.BinanceAPIException:
            print 'order already filled'
            break
           
            