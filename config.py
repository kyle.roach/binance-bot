#
# config.py
#
import time


# Lists
market_pairs_list = []
asset_list = []

# JSON Database Configs
minimum_quantity = {} 
minimum_order_size = {}
step_size = {}
standard_deviation = {}
ten_day_moving_average = {}
time_of_json_update = {}

# Asset Log Configs
time_hourly_update = time.time();
time_daily_update = time.time();
usd_value_hourly_update = 0;
usd_value_daily_update = 0;

# Candle Configs
last_candle = {} # 10 min
current_candle = {} # 10 min
candle_open_price = {}
candle_open_time = 0
candle_close_price = {}
candle_updated = False

# Market configs
qty_asset = {}
market_order_history = {}

transfer_dust = False

# Timing configs
start_time = 0;
end_time = 0;

# Constants
TEN_MINUTES = 600
ONE_HOUR = 3600
TWENTY_FOUR_HOURS = 86400

# terminate mainloop
termination_requested = False
