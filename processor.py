# 
# processor.py
#

import api
import market_logger
import config
import time


BUY_RATIO = 0.99
SELL_RATIO = 0.99


def mainLoop():
    market_logger.update_configs()
        
    results = make_decision()
    if results != -1:
        for market_pair in config.market_pairs_list:
            if (results[market_pair] == 1):
                buy(market_pair)
            elif(results[market_pair] == 2):
                sell(market_pair)
            elif(results[market_pair] == 0):
                continue
            else:
                break
        
        finisher();
    
def make_decision(): #return 1=buy, 2=sell, 0=continue
    decisions = {}
    
    if not config.last_candle or not config.current_candle:
        return -1
    
    if not config.candle_updated:
        return -1
    
    for market_asset in config.asset_list:
        if market_asset == 'USDT':
            print (market_asset + ' ' + str(config.qty_asset[market_asset]))
        else:
            print (market_asset + ' ' + str(config.qty_asset[market_asset]) + ' usd = ' + str(config.qty_asset[market_asset]*config.candle_close_price[market_asset + 'USDT']))
    
    for market_pair in config.market_pairs_list:
        
        buy1 = config.last_candle[market_pair] < 0
        buy2 = config.current_candle[market_pair] > abs(config.last_candle[market_pair]) * 0.75
        buy3 = config.qty_asset['USDT'] * BUY_RATIO > config.minimum_order_size[market_pair]
        
        sell1 = config.current_candle[market_pair] < 0
        sell2 = abs(config.current_candle[market_pair]) > config.last_candle[market_pair]
        sell3 = config.qty_asset[market_pair.replace('USDT', '')]*api.get_market_price(market_pair)*SELL_RATIO> config.minimum_order_size[market_pair]
                
        print (market_pair + ', ' +str(config.last_candle[market_pair]*100) + '%, ' + str(config.current_candle[market_pair]*100) + '%')
        if buy1 and buy2 and buy3:
            decisions[market_pair] = 1
            
        elif sell1 and sell2 and sell3:
            # cancel any active stop loss orders
            stop_order_list = api.get_open_orders(market_pair)
            if stop_order_list:
                for stop_order in stop_order_list:
                    api.cancel_open_order(market_pair, int(stop_order['orderId']))
                    
            decisions[market_pair] = 2
            
        else:
            decisions[market_pair] = 0
    
    return decisions

def buy(market_pair):
    if config.qty_asset['USDT'] * BUY_RATIO > config.minimum_order_size[market_pair]:
        print ('buy ' + market_pair)
    
        quantity = (BUY_RATIO*config.qty_asset['USDT'])/config.candle_close_price[market_pair]
    
        market_order = api.market_buy(market_pair, quantity)
        print market_order

        config.market_order_history[market_pair] = market_order
    
        # update balances
        config.qty_asset['USDT'] = api.get_balance('USDT')
        config.qty_asset[market_pair.replace('USDT', '')] = api.get_balance(market_pair.replace('USDT', ''))
    

def sell(market_pair):
    if api.get_balance(market_pair.replace('USDT', ''))*api.get_market_price(market_pair)*SELL_RATIO> config.minimum_order_size[market_pair]:
        print ('sell ' + market_pair)
    
        quantity = SELL_RATIO*config.qty_asset[market_pair.replace('USDT', '')]

        order = api.market_sell(market_pair, quantity)
        print order
    
        # delete corresponding market_order from history
        if market_pair in config.market_order_history:
            del config.market_order_history[market_pair]
    
        # update balances
        config.qty_asset['USDT'] = api.get_balance('USDT')
        config.qty_asset[market_pair.replace('USDT', '')] = api.get_balance(market_pair.replace('USDT', ''))
    

# we will update stop losses and any stats here
def finisher():
    
    # if nothing happened we will sleep until it is time to make another trading decision, currently
    # that time is 10 minutes
    if not config.candle_updated:
        time_till_next_check = config.candle_open_time - (time.time()-config.TEN_MINUTES)
        time.sleep(time_till_next_check)
        return
    
    # create or update any existing stop losses
    # update_stop_loss()
   
    # transfer leftover holdings to bnb, can only be done once every 24h
    # if config.transfer_dust:
    #     transfer_dust()
    # need to fix ^

    market_logger.update_asset_log()

def update_stop_loss():
    
    # returns market_pairs that were recently ordered
    for market_pair in config.market_order_history.keys():
        print market_pair
        market_order =  config.market_order_history[market_pair]
        
        # check if position still holds, else we will delete the order history as we can conclude that the previous
        # stop loss has been filled
        print api.get_balance(market_pair.replace('USDT', ''))*api.get_market_price(market_pair)*SELL_RATIO
        print config.minimum_order_size[market_pair]
        if api.get_balance(market_pair.replace('USDT', ''))*api.get_market_price(market_pair)*SELL_RATIO> config.minimum_order_size[market_pair]:
            
            # only update stop loss if position has increased 
            if config.current_candle[market_pair] > 0:
                
                # cancel current stop loss
                stop_order_list = api.get_open_orders(market_pair)
                if stop_order_list:
                    for stop_order in stop_order_list:
                        api.cancel_open_order(market_pair, int(stop_order['orderId']))
                
                quantity = SELL_RATIO*config.qty_asset[market_pair.replace('USDT', '')]
    
                # set stop price to price at start of candle
                stop_price = float(market_order['fills'][0]['price']) - config.current_candle[market_pair]
                print ('stop_price = ' + str(stop_price))
    
                # check that stop price is valid, else we will just sell the pair instead of setting a stop loss
                if stop_price < api.get_market_price(market_pair):
                    print 'UPDATING STOP LOSS'
                    order = api.set_stop_loss(market_pair, stop_price, quantity)
                    if(order == -1):#means price changed and stop loss was set to high 
                        sell(market_pair)
                    print order
                else:
                    sell(market_pair)
                    
                #print stop_loss_order
                print api.get_open_orders(market_pair)
    
            else:
                print 'NOT UPDATING STOP LOSS'
    
        else:
            print 'Stop Loss has been executed!'
            del config.market_order_history[market_pair]


def transfer_dust():    

    for market_asset in config.asset_list:
        if(market_asset != 'USDT'):
            market_pair = market_asset + 'USDT'
            if config.qty_asset[market_asset]*api.get_market_price(market_pair) < config.minimum_order_size[market_pair]:
                dust_transfer = api.transfer_dust(market_asset)
                print dust_transfer

    config.transfer_dust = False
        
